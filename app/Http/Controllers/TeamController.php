<?php

namespace App\Http\Controllers;

use App\Team;
use Illuminate\Http\Request;
use App\Http\Requests\TeamRequest;
use App\Http\Requests\CompetitionRequest;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Team $team)
    {
        $sort = $request->input('sort','name');
        $order = $request->input('order','desc');
        
        $result = $team->with(['rival','competitions','country'])->withCount('competitions');
        $fields = array_merge(['competitions_count'],$team->getFillable());

        if(in_array($sort,$fields) && in_array($order,['asc','desc'])){
            $result->orderBy($sort,$order);
        }
        
        return $result->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeamRequest $request)
    {
        if($team = Team::create($request->all()))
        {
            return $this->fullTeam($team->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function show(Team $team)
    {
        return $this->fullTeam($team->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function update(TeamRequest $request, Team $team)
    {
        if($team->update($request->all()))
        {
            return $this->fullTeam($team->id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function destroy(Team $team)
    {
        $team->delete();
        return ['deleted' => true];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function addCompetition(CompetitionRequest $request,Team $team)
    {
        if($team->competitions()->create($request->validated()))
        {
            return $this->fullTeam($team->id);
        }
    }

    /**
     * get a team's full info
     */
    private function fullTeam($teamId)
    {
        return Team::with(['rival','competitions','country'])->withCount('competitions')->find($teamId);
    }
}
