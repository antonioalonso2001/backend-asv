<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{

    /**
     * The attributes that are mass fillable.
     *
     * @var array
     */
    protected $fillable = ['name','image','country_id','rival_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];  

    /**
     * Rival relationship
     */
    public function rival()
    {
        return $this->hasOne('App\Team','id','rival_id');
    }

    /**
     * Competition's relationship
     */
    public function competitions()
    {
        return $this->hasMany('App\Competition','team_id');
    }

    /**
     * country relationship
     */
    public function country()
    {
        return $this->hasOne('App\Country','id','country_id');
    }
}
