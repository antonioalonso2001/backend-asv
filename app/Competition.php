<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Competition extends Model
{

    /**
     * The attributes that are mass fillable.
     *
     * @var array
     */    
    protected $fillable = ['team_id','date'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at','team_id'
    ]; 

    /**
     * team relationship
     */
    public function team()
    {
        return $this->hasOne('App\Team');
    }
}
