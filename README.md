# backend-asv

El Backend de la app está desarrollado en Laravel, para poder ejecutarlo en solitario necesita lo siguiente:
* PHP 7.x
* Mysql
* nginx o Apache
* composer

## Como instalarlo

Clona el repositorio

```
git clone https://antonioalonso2001@bitbucket.org/antonioalonso2001/backend-asv.git
```

Entra a la carpeta **backend-asv** recién creada y ejecuta:

```
composer install
php artisan key:generate
php artisan migrate:refresh --force --seed
```

Una vez instalado correctamente el backend expondrá los siguientes endpoints:

* **GET** /api/teams  nos da la lista de equipos, admite los query params:
  * sort: ordena por los siguientes campos: **name** y **competitions_count**
  * order: cambia el orden a **asc** ascendente o **desc** descendente
* **POST** /api/teams Crea un nuevo equipo
* **GET** /api/teams/_id_ nos da los datos de un equipo en concreto
* **POST** /api/teams/_id_ actauliza un equipo concreto
* **DELETE** /api/teams/_id_ borra un equipo concreto
* **POST** /api/teams/_id_/competitions añade una competición a un equipo concreto
* **GET** /api/countries obtine el listdo de todos los paises
