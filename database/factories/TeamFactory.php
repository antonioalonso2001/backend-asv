<?php

use App\Country;
use Faker\Generator as Faker;

$factory->define(App\Team::class, function (Faker $faker) {
    return [
        'name' => $faker->text(24),
        'image' => $faker->imageUrl(200,200,'sports'),
        'country_id' => Country::all()->random(1)->first()->id,
        'rival_id' => $faker->numberBetween(0,7)
    ];
});
