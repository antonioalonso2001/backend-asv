<?php

use App\Team;
use Faker\Generator as Faker;

$factory->define(App\Competition::class, function (Faker $faker) {
    return [
        'date' => $faker->date(),
        'team_id' => Team::all()->random(1)->first()->id,
    ];
});
