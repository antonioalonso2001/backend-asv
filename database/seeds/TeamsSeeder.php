<?php

use Illuminate\Database\Seeder;

class TeamsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $numberOfTeams = 7;

        $teams = factory(App\Team::class, $numberOfTeams)->create();

        $competitions = factory(App\Competition::class,12)->create();
    }
}
