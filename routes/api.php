<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'teams'],function(){
    //get the list of teams
    Route::get('/','TeamController@index');

    //create a new team
    Route::post('/','TeamController@store');

    //get a team with id team
    Route::get('/{team}','TeamController@show')->where('team', '[0-9]+');

    //update a team with id team
    Route::post('/{team}','TeamController@update')->where('team', '[0-9]+');

    //delete a team with id team
    Route::delete('/{team}','TeamController@destroy')->where('team', '[0-9]+'); 

    //add a competition to a team
    Route::post('/{team}/competitions','TeamController@addCompetition')->where('team', '[0-9]+');
});

Route::group(['prefix' => 'countries'],function(){
    Route::get('/','CountryController@index');
});
